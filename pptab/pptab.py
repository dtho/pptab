from __future__ import print_function
#
# pptab - parse plain-text contact information
#
from future import standard_library
standard_library.install_aliases()
import argparse
import json
from nameparser import HumanName
import phonenumbers
import sys
from urllib.parse import urlparse
import usaddress
import validate_email

import logging
#log_level = logging.DEBUG
log_level = logging.ERROR
logging.basicConfig(stream=sys.stderr, level=log_level)

# track parsing progress
parse_status = {
    'path': None,
    'last_line_parsed': -1,
    'lines': []
    }

def empty_line_p(x):
    return not x.strip()

def make_contact():
    """Return a contact object."""
    return {
        'name': None,
        'structuredname': None,
        'address': None,
        'emails': [],
        'notes': [],
        'phones': [],
        'uri': None
    }

# OrderedDict([('AddressNumber', u'5478'), ('StreetNamePreDirectional', u'W'), ('StreetName', u'Everett'), ('StreetNamePostType', u'Ave\n')])
# OrderedDict([('PlaceName', u'Fresno'), ('StateName', u'CA\n')])
def grab_address (parse_status):
    """Return a tuple of OrderedDict objects. If unable to grab something resembling an address, return None."""
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    #logging.debug( "line",line )
    formatted_street_address = None
    street_address1 = None
    street_address2 = None
    # either start of the 'street portion' of a street address or we've encountered a mess
    addr_obj = None
    try:
        # test if street address by looking at first line
        street_address1 = parse_street_address_street(line)
        # if it looks like a street address, try and parse the entire address
        if street_address1:
            # Note that it's possible that it's all on one line
            parse_status["last_line_parsed"] = 1+parse_status["last_line_parsed"]
            # It's also possible more is on the next line
            try:
                next_line=parse_status['lines'][1+next_line_number]
                street_address2 = parse_street_address_city_state_zip(next_line)
                if street_address2:
                    parse_status["last_line_parsed"] = 1+parse_status["last_line_parsed"]
            except:
                street_address2 = None
    except:
        street_address1 = None
    # Since the trailing \n that sometimes is left in for values is
    # not desirable, trim the value for each key. If this isn't a good
    # idea, document why.
    if street_address1:
        for key, value in list(street_address1.items()):
            street_address1[key] = street_address1[key].rstrip()
    if street_address2:
        for key, value in list(street_address2.items()):
            street_address1[key] = street_address2[key].rstrip()
    if street_address1 or street_address2:
        return street_address1, street_address2
    else:
        return None

def grab_email (parse_status):
    """Return None if unable to parse an email address at the current line."""
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    maybe_email=line.rstrip()
    is_valid = validate_email.validate_email(maybe_email,
                                             check_mx=False)
    if is_valid:
        parse_status["last_line_parsed"] = next_line_number
        return maybe_email
    else:
        return None

def grab_email_note_phone_uri_or_addr (parse_status,contact):
    """Return None if unable to find an address, email, note, phone, or URI."""
    last_line_parsed = parse_status["last_line_parsed"]
    # try for phone # first
    phone = grab_phone(parse_status)
    logging.debug( "phone* %r",phone )
    if phone:
        contact['phones'].append(phone)
        return phone
    else:
        email = grab_email(parse_status)
        logging.debug( "email* %r",email )
        if email:
            contact['emails'].append(email)
            return email
        else:
            note = grab_note(parse_status)
            logging.debug( "note* %r",note )
            if note:
                contact['notes'].append(note)
                return note
            else:
                uri = grab_uri(parse_status)
                logging.debug( "uri* %r",uri )
                if uri:
                    contact['uri'] = uri
                    return uri
                else:
                    address = grab_address(parse_status)
                    if address:
                        contact['address'] = address
                        return address
                    else:
                        return None

# Consume from current position to next non-empty line. Parse contact information at that point.
# see above for key/value structure of parse_status
def grab_entry(parse_status):
    """Return a contact object."""
    # store accumulated data in a contact object
    contact = make_contact()
    # pass empty line(s)
    move_past_empty_lines(parse_status)
    # look for name (a single line)
    name = grab_name(parse_status)
    if name:
        contact['name'] = name[0]
        contact['structuredname'] = name[1]
    else:
        sys.exit("Encountered entry w/o name")
    # look for an empty line (or no lines) following the name
    if ( parse_status['last_line_parsed'] + 1 == len(parse_status['lines'] ) ):
        return contact
    else:
        next_line_number = parse_status['last_line_parsed'] + 1
        next_line = parse_status['lines'][ next_line_number ]
        if empty_line_p(next_line):
            return contact
    # if non-empty line follows the name, look for email, phone, uri, or address lines
    number_of_tries = 0
    max_entry_tries = 10;
    while (grab_email_note_phone_uri_or_addr(parse_status,contact) and
           (parse_status['last_line_parsed'] < len(parse_status['lines']) - 1)):
        noop = 1        # noop line
        number_of_tries = number_of_tries + 1
        #logging.debug( parse_status,len(parse_status['lines']) )
        if ( number_of_tries == max_entry_tries ):
            logging.debug( "Broken at entry" )
            sys.exit("Broken at entry")
    # if we're here and don't have an email, note, phone_number, or URI, this is an error... (a name by itself isn't a legitimate entry)
    return contact

def grab_name(parse_status):
    """Return a string."""
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    name=line.rstrip()
    if name:
        parse_status["last_line_parsed"] = next_line_number
        # Try to parse name and generated a structured name (first, last, honorific, middle, ...)
        # - HumanName(name) is not JSON serializable so return as dict
        structured_name = HumanName(name)
        return (name,structured_name.as_dict())
    else:
        return None

def grab_note (parse_status):
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    if (line[0:5].upper() == 'NOTE:'):
        note = line[5:].rstrip()
        if note:
            parse_status["last_line_parsed"] = next_line_number
            return note
        else:
            return None

def grab_phone (parse_status):
    """Return None if unable to parse a phone number at the current line."""
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    try:
        # add +1 if not present at start of number
        phone_test_line = line
        if (not line[0:2] == "+1"):
            phone_test_line = "+1" + phone_test_line
        phone_number_obj = phonenumbers.parse(phone_test_line)
        if phonenumbers.is_possible_number(phone_number_obj):
            phone_number = line.rstrip()
            parse_status["last_line_parsed"] = next_line_number
            return phone_number
        else:
            return None
    except phonenumbers.phonenumberutil.NumberParseException:
        return None;

def grab_uri (parse_status):
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    parse_result = urlparse(line.rstrip())
    if not (parse_result.scheme == '') and not (parse_result.netloc == ''):
        parse_status["last_line_parsed"] = next_line_number
        uri = parse_result
        return uri
    else:
        return None

def init_parse_status(ps,path):
    ps['last_line_parsed']=-1
    ps['lines']=None
    ps['path']=path
    # FIXME: add sanity check for existence of file
    f=open(path,'r')
    ps['lines']=f.readlines()
    f.close()

def move_past_empty_lines (parse_status):
    """Maintain parse_status object while moving through empty lines."""
    next_line_number=1+parse_status['last_line_parsed']
    line=parse_status['lines'][next_line_number]
    while empty_line_p(line):
        next_line_number=1+next_line_number
        line=parse_status['lines'][next_line_number]

def parse_street_address_street(addr_lines):
    street_address = None
    addr_tuple = usaddress.tag(addr_lines)
    if addr_tuple:
        addr_obj = addr_tuple[0]
        if addr_obj:
            if addr_obj['AddressNumber'] and addr_obj['StreetName']:
                return addr_obj
            else:
                return None
        else:
            return None
    else:
        return None

def parse_street_address_city_state_zip(addr_lines):
    street_address = None
    addr_tuple = usaddress.tag(addr_lines)
    if addr_tuple:
        addr_obj = addr_tuple[0]
        if addr_obj:
            if addr_obj['PlaceName'] and addr_obj['StateName']:
                return addr_obj
            else:
                return None
        else:
            return None
    else:
        return None

def parse_book(path):
    """For development work"""
    try:
        return parse_bookProduction(path)
    except SystemExit:
        pass

def parse_bookProduction(path):
    """External interface for pptab"""
    init_parse_status(parse_status,path)
    return grab_entry(parse_status)

#
# entry point for external/shell invocation
#
def main():
    """Handle command-line invocation."""
    parser = argparse.ArgumentParser(description="This is pptab")
    # parser.add_argument("-f",
    #                     help="absolute path to file with address",
    #                     action="store",
    #                     dest="addr_file",
    #                     type=str)

    #parser.add_argument('-v', '--version', action='version', version=version.version)
    parser.add_argument("input_files", help="an input file",
                        # keep nargs as we may want to accept multiple PDFs as input at some point
                        nargs=1,
                        type=str)
    args = parser.parse_args()
    addr_file_spec = args.input_files[0]
    try:
        return_val = parse_book(addr_file_spec)
        print(json.dumps(return_val))
    except Exception as e:
        #lg.error("Crash and burn")
        #lg.error(sys.exc_info()[0])
        print("Crash and burn")
        print(sys.exc_info()[0])
        raise

if __name__ == "__main__":
    main()

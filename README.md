# pptab

*Parse plain text address book entries*

---

`pptab` is a tool to parse a plain-text "address book entry". The current version is intended to be used with Python 3.

The addressbook entry can be a mish-mash which might include a personal name and/or an institutional name, a street address, a phone number, an email, and/or miscellaneous notes.


## Getting started

### 1. Check that you're working with Python 3

Invoking `$ python3 -m pip --version` should yield something along the lines of `pip 18.1 from /usr/lib/python3/dist-packages/pip (python 3.7)`.

### 2. Install  dependencies:

```
pip install phonenumbers
pip install nameparser
pip install usaddress
pip install validate_email
```

Use `python3 -m pip install [package]` if unsure regarding which Python planet you're currently on.


### 3. Install pptab

```
$ python3 ./setup.py install
```

## Use pptab
Try out `pptab` at the command-line with a file containing address book content in plain text.

Example:

```
$ echo -e "Lisa Smith\n5123 W Even Ave\nSomeTown, CA\n543 222 3456\nnote: A friend of JoeJoe's\nnote: Take Highway 1 north and exit at Main St.\n" > ./plain-text-abook-test-a.txt

$ cat ./plain-text-abook-test-a.txt 
Lisa Smith
5123 W Even Ave
SomeTown, CA
543 222 3456
note: A friend of JoeJoe's
note: Take Highway 1 north and exit at Main St.

$ pptab ./plain-text-abook-test-a.txt 
{"name": "Lisa Smith", "structuredname": {"title": "", "first": "Lisa", "middle": "", "last": "Smith", "suffix": "", "nickname": ""}, "address": [{"AddressNumber": "5123", "StreetNamePreDirectional": "W", "StreetName": "Even", "StreetNamePostType": "Ave", "PlaceName": "SomeTown", "StateName": "CA"}, {"PlaceName": "SomeTown", "StateName": "CA\n"}], "emails": [], "notes": [" A friend of JoeJoe's", " Take Highway 1 north and exit at Main St."], "phones": ["543 222 3456"], "uri": null}
```
